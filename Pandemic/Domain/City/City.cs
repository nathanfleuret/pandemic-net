﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pandemic.Domain.City
{
    public class City
    {
        private readonly CityName name;
        private int infectionLevel;

        public City(CityName name)
        {
            this.name = name;
            infectionLevel = 0;
        }

        public void infect()
        {
            if (infectionLevel < 3)
            {
                infectionLevel += 1;
            }
        }
        public int getInfectionLevel() => infectionLevel;

        public void setInfectionLevel(int infectionLevel)
        {
            this.infectionLevel = infectionLevel;
        }

        public override String ToString()
        {
            return "City{" +
                    "name=" + name +
                    '}';
        }
    }
}
