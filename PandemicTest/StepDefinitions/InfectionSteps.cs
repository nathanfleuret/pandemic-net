using Microsoft.VisualStudio.TestTools.UnitTesting;
using Pandemic.Domain.City;
using System;
using System.Collections.Generic;
using TechTalk.SpecFlow;

namespace PandemicTest.StepDefinitions
{
    [Binding]
    public class InfectionSteps
    {
        Dictionary<CityName, City> map = new Dictionary<CityName, City>() {
            { CityName.PARIS, new City(CityName.PARIS) } 
        };

        //les parenthèses permettent d'envoyer le (match) comme argument
        //on aurait pu utiliser (Paris) pour ne filtrer que sur Paris, bien garder les parenthèses
        [StepArgumentTransformation(@"(.*)")]
        public City City(CityName cityName) => map.GetValueOrDefault(cityName);

        [Given(@"(.*) has not been infected")]
        public void GivenParisHasNotBeenInfected(City city) => city.setInfectionLevel(0);

        [When(@"(.*) is infected")]
        public void WhenParisIsInfected(City city) => city.infect();

        //permet de matcher la premiere ou la seconde expression: (?:increase to|remain)
        [Then(@"(.*) infection level should (?:increase to|remain) (\d)")]
        public void ThenParisInfectionLevelShouldBe(City city, int expectedInfectionLevel)
        {
            Assert.AreEqual(city.getInfectionLevel(), expectedInfectionLevel);
        }

        //permet de matcher le singulier et le pluriel de 'time', soit un 's' optionnel: time(?:s)?
        [Given(@"(.*) has been infected (.*) time(?:s)?")]
        public void GivenParisHasBeenInfected(City city, int infectionLevel)
        {
            for (int i = 0; i < infectionLevel; i++)
            {
                city.infect();
            }
        }        
    }
}
